<%@page import="java.util.Enumeration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title>OpenID login</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<%-- CSS --%>
<link href="<c:url value="/resources/css/styles.css" />" rel="stylesheet" media="screen">

<script>
	var CURRENT_LOCALE = '<c:out value="${rc.locale.language}"/>';
	var CONTEXT_ROOT = '<c:out value="${pageContext.request.contextPath}"/>';
	var SERVER_HOST = '<c:out value="${pageContext.request.serverName}"/>'; 
	var SERVER_PORT = '<c:out value="${pageContext.request.serverPort}"/>';
	var PROTOCOL = '<c:out value="${pageContext.request.scheme}"/>';
	var CONTEXT_URL = PROTOCOL + "://" + SERVER_HOST + ":" + SERVER_PORT + CONTEXT_ROOT;
	var BASE_URL = PROTOCOL + "://" + SERVER_HOST + ":" + SERVER_PORT;
	var REFRESH_INTERVAL = ${refreshInterval};
	var DANGER_THRESHOLD = ${dangerThreshold};
	var SUCCESS_THRESHOLD = ${successThreshold};	
</script>
</head>
<body>		
	<h3>Welcome, ${firstName} ${middleName} (<sec:authentication property="principal.username" />)</h3>
	<a href="/openid-login/j_spring_security_logout">Logout</a></li>
</body>
</html>