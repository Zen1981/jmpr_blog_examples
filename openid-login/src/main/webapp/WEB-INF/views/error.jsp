<%@page import="java.util.Enumeration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<head>
<title><spring:message code="general.application.title" /></title>
<link type="text/css" media="all" rel="stylesheet" href="<c:url value="/resources/css/main.css" />" />
</head>
<body>
	<h1>
		<spring:message code="login.error.title" />
	</h1>
	<div>
		<a href="<c:url value="/login" />"><spring:message code="general.back"/></a>
	</div>
</body>