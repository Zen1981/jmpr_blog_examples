<%@page import="java.util.Enumeration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<head>
<title>OpenID login</title>
<link type="text/css" media="all" rel="stylesheet" href="<c:url value="/resources/css/styles.css" />" />
</head>
<body class="body-login">
	<c:url var="openIDLoginUrl" value="/j_spring_openid_security_check" />
	<c:url var="googleLogoUrl" value="/resources/img/google.png" />

	<form action="${openIDLoginUrl}" method="post" class="text-centered">
		<input name="openid_identifier" type="hidden" value="https://www.google.com/accounts/o8/id" /> <input class="google-login-image" type="image" alt="Google login" value="Sign with Google"
			src="${googleLogoUrl}" />
	</form>
	<c:choose>
		<c:when test="${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].getClass().name eq 'org.springframework.security.core.userdetails.UsernameNotFoundException'}">
			<span id="message_error"> <c:out value="${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}" />
			</span>
		</c:when>
	</c:choose>
</body>