package com.jmpr.blog.openid.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.openid.OpenIDAttribute;
import org.springframework.security.openid.OpenIDAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/secure")
public class PrivateController {
	
	private static Logger logger = LoggerFactory.getLogger(PrivateController.class);
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest request) {
		logger.info("Successfully logged in");
		
		// Access to token returned by OpenID Authentication
		OpenIDAuthenticationToken token =
			    (OpenIDAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
			List<OpenIDAttribute> attributes = token.getAttributes();
		model.addAttribute("firstName", attributes.get(1).getValues().get(0));
		model.addAttribute("middleName", attributes.get(2).getValues().get(0));
			
		return "secure/home";
	}

}