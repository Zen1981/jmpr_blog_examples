package com.jmpr.blog.openid.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PublicController {
	private static Logger logger = LoggerFactory.getLogger(PublicController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest request) {
		logger.info("Home page. Locale = {}", locale.toString());
		
		return "login";
	}
	
	@RequestMapping(value = "/loginerror")
	public String loginerror(Model model) {
		logger.info("Request GET /loginerror");
		
		model.addAttribute("error_login", true);		
		return "login";
	}	

	@RequestMapping(value = "/login")
	public String login(Model model, HttpServletRequest request) {
		logger.info("Request GET /login");
		
		return "login";
	}
}