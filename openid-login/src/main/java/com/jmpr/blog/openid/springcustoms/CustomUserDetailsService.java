package com.jmpr.blog.openid.springcustoms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAttribute;
import org.springframework.security.openid.OpenIDAuthenticationToken;


@SuppressWarnings("deprecation")
public class CustomUserDetailsService implements UserDetailsService, AuthenticationUserDetailsService<OpenIDAuthenticationToken> {

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// Not used
		return null;
	}

	public UserDetails loadUserDetails(OpenIDAuthenticationToken token) throws UsernameNotFoundException {
		boolean isAllowedDomain = false;
		String email = null;
		
		// Get token attributes
		List<OpenIDAttribute> attributes = token.getAttributes();
		for (OpenIDAttribute openIDAttribute : attributes) {
			if ("email".equals(openIDAttribute.getName())) {
				if (openIDAttribute.getValues().get(0).endsWith("beeva.com")) {
					isAllowedDomain = true;
					email = openIDAttribute.getValues().get(0);
				}
			}
		}
		
		CustomUserDetails ud = new CustomUserDetails();
		if (isAllowedDomain) {
			ud.setAccountNonExpired(true);
			ud.setUsername(email);
			ud.setEnabled(true);
			ud.setCredentialsNonExpired(true);
			ud.setPassword(null);
			
			// Authorities
			List<GrantedAuthorityImpl> authorities = new ArrayList<GrantedAuthorityImpl>();
			authorities.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
			
			ud.setAuthorities(authorities);
			
		} else {
			throw new UsernameNotFoundException("Your account is not allowed in this application");
		}
		return (UserDetails) ud;
	}
}